
(function ($, Drupal, window, document, undefined) {
(function($) {
Drupal.behaviors.flex_navi_set = {
  attach: function (context, settings) {
     $('.flex-prev').click(function()
        {
          $('.view-sliderundermenu .flexslider').flexslider('prev');
          

        }
    );
    $('.flex-next').click(function()
        {
          $('.view-sliderundermenu .flexslider').flexslider('next');
 
        }
    );
  }
};
})(jQuery);

})(jQuery, Drupal, this, this.document);
